﻿using System.Collections;
using UnityEngine;

namespace AgentX
{
    public class Routines
    {
        public static IEnumerator Wait(float time)
        {
            var t = 0f;
            while (t < time)
            {
                yield return null;
                t += Time.deltaTime;
            }
        }
    }
}
