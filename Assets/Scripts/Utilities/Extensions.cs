﻿using UnityEngine;
using static UnityEngine.Quaternion;

namespace AgentX
{
    public static class Extensions
    {
        public static void RotateYaw(ref this Vector3 vector, float yaw)
        {
            if (yaw != 0f && vector != Vector3.zero) vector = Euler(0, yaw, 0) * vector;
        }
    }
}
