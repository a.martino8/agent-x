﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Vector3;

namespace AgentX
{
    [Serializable]
    public class AStarSolver
    {
        [SerializeField] private float closeDistanceTreshold = 1;
        private readonly CostSortedList frontier = new CostSortedList();
        private readonly List<Node> explored = new List<Node>();

        /// <summary>
        /// Applica A* per ritornare il path ottimo per il goal.
        /// Se il goal non è attualmente raggiungibile, applica ricorsivamente A* ritornare il path verso, se esiste, un nodo che:
        ///     - non è stato ancora (fisicamente) visitato (perché potrebbe rivelare la presenza di altri nodi)
        ///     - è quello più prossimo al goal in termine di funzione di valutazione F (*: vedi nota)
        /// Se anche un nodo simile non viene trovato, allora ritorna null ad indicare che il goal non è raggiungibile in alcun modo
        /// 
        /// *: è interessante vedere come l'agente si comporti in modo diverso se prendessimo quello più prossimo in termini di H o sfruttando altri criteri
        /// </summary>
        public List<Node> Solve(Node startingNode, Vector3 targetPosition, out bool isTargetFound)
        {
            frontier.Clear();
            explored.Clear();
            
            isTargetFound = Distance(startingNode.position, targetPosition) < closeDistanceTreshold;
            if (isTargetFound)
            {
                explored.Add(startingNode);
                return GetPath(startingNode);
            }

            frontier.Add(F(startingNode, startingNode, targetPosition), startingNode);

            Node unvisitedNode = null;
            var bestFitNodeHeuristic = 9999999f;
            while (frontier.Count > 0)
            {
                var currentNode = frontier.TakeFirst();
                //var currentNodeHeuristicValue = Distance(currentNode.position, targetPosition);
                var currentNodeHeuristicValue = F(currentNode.visitingParent ?? currentNode, currentNode, targetPosition);


                if (!currentNode.wasVisited && (unvisitedNode == null || currentNodeHeuristicValue < bestFitNodeHeuristic))
                {
                    bestFitNodeHeuristic = currentNodeHeuristicValue;
                    unvisitedNode = currentNode;
                }

                explored.Add(currentNode);
                foreach (var node in currentNode.connectedNodes)
                {
                    if (!explored.Contains(node))
                    {
                        if (!frontier.Values.Contains(node))
                        {
                            if (Distance(node.position, targetPosition) < closeDistanceTreshold)
                            {
                                node.visitingParent = currentNode;
                                explored.Add(node);
                                isTargetFound = true;
                                return GetPath(startingNode);
                            }
                            else
                            {
                                node.visitingParent = currentNode;
                                frontier.Add(F(currentNode, node, targetPosition), node);
                            }
                        }
                        else
                        {
                            var index = frontier.IndexOfValue(node);
                            var prevCost = frontier.Keys[index];
                            var newCost = F(currentNode, node, targetPosition);
                            if (newCost < prevCost)
                            {
                                frontier.RemoveAt(index);
                                frontier.Add(newCost, node);
                                node.visitingParent = currentNode;
                            }
                        }
                    }
                }

            }

            if (unvisitedNode != null)
            {
                isTargetFound = false;
                return Solve(startingNode, unvisitedNode.position, out _);
            }

            isTargetFound = false;
            return null;
        }

        public List<Node> GetPath(Node presenceNode)
        {
            if (explored.Count == 0) return new List<Node>();
            var solution = new List<Node>();
            var focusNode = explored.Last();
            var s = "EXPLORED: ";
            foreach (var n in explored) s += $"{n.id}, ";
            s = $"{s} END";
            while (focusNode != presenceNode && focusNode != null)
            {
                solution.Add(focusNode);
                focusNode = explored.FindLast((node) => node == focusNode.visitingParent);
            }
            solution.Reverse();
            s = $"{s}\tSOLUTION: {presenceNode.id}";
            foreach (var n in solution) s += $"-->{n.id}";
            //Debug.Log(s);
            return solution;
        }

        private float F(Node from, Node to, in Vector3 targetPosition)
        {
           to.cost = from.cost + Distance(from.position, to.position);
           return to.cost + Distance(to.position, targetPosition);
        }

        public class CostSortedList : SortedList<float, Node>, IEnumerable<Node>
        {
            IEnumerator<Node> IEnumerable<Node>.GetEnumerator() => Values.GetEnumerator();
            public Node TakeFirst()
            {
                var first = ((SortedList<float, Node>)this).ElementAt(0).Value;
                if (first != null) RemoveAt(0);
                return first;
            }

            public CostSortedList() : base(new DuplicateKeyComparer<float>()) { }

            public class DuplicateKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
            {
                public int Compare(TKey x, TKey y)
                {
                    int result = x.CompareTo(y);
                    if (result == 0) return 1;
                    else return result;
                }
            }
        }

    }
}
