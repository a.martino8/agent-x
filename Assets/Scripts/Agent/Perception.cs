using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Mathf;
using static UnityEngine.Physics;
using static UnityEngine.Vector3;

namespace AgentX.Agent
{
    [Serializable]
    public class Perception
    {
        public event Action<Vector3> PointFound;
        [SerializeField] private Transform transform = default;
        [SerializeField] private LayerMask obstaclesLayer = default;
        [SerializeField] private LayerMask goalLayer = default;
        [SerializeField] private Transform target = default;
        [SerializeField] private int halfWidth = 90;
        [SerializeField] private float density = 1;
        [SerializeField] private float depth = 10f;
        [SerializeField] private float scanPrecision = 10f;
        [SerializeField] private float scanInterval = 0.25f;
        private readonly Dictionary<int, Obstacle> obstacles = new Dictionary<int, Obstacle>();
        private bool isTargetPerceived;

        public IEnumerator Perceive()
        {
            while (true)
            {
                if (!isTargetPerceived) CastTargetHit();
                ForEachSightLine(CastHit);
                yield return Routines.Wait(scanInterval);
            }
        }

        public void ForEachSightLine(Action<Vector3> action)
        {
            var inverseDensity = 1 / density;
            var direction = transform.forward;
            direction.RotateYaw(-halfWidth);
            var nRays = CeilToInt(halfWidth * 2 * density);
            for (int i = 0; i < nRays; i++)
            {
                action(direction);
                direction.RotateYaw(inverseDensity);
            }
        }

        private void CastHit(Vector3 castDirection)
        {
            if (Raycast(transform.position, castDirection, out var hit, depth, obstaclesLayer))
            {
                var targetID = hit.transform.gameObject.GetInstanceID();

                if (obstacles.TryGetValue(targetID, out Obstacle obstacle))
                {
                    if (obstacle.IsVertexComplete || obstacle.IsNormalComplete || obstacle.ContainsNormal(hit.normal)) return;
                    else obstacle.SetNormal(hit.normal);
                }
                else
                {
                    obstacle = new Obstacle();
                    obstacle.SetNormal(hit.normal);
                    obstacles.Add(targetID, obstacle);
                }

                var scanDirection = hit.normal;

                scanDirection.RotateYaw(-90);
                ComputeVertexInformation(hit, scanDirection, castDirection, obstacle);

                scanDirection.RotateYaw(180);
                ComputeVertexInformation(hit, scanDirection, castDirection, obstacle);
            }
        }

        private void CastTargetHit()
        {
            var adaptedTargetPosition = target.position;
            adaptedTargetPosition.y = transform.position.y;
            var direction = (adaptedTargetPosition - transform.position).normalized;
            if (Raycast(transform.position, direction, out var hit, depth, goalLayer) && !Raycast(transform.position, direction, hit.distance, ~goalLayer))
            {
                isTargetPerceived = true;
                PointFound?.Invoke(adaptedTargetPosition);
            }
        }

        private void ComputeVertexInformation(RaycastHit hit, Vector3 direction, Vector3 originalDirection, Obstacle obstacle)
        {
            var referencePoint = hit.point + direction * depth;
            var vertexType = Vertex.GetVertexType(referencePoint, hit.transform);

            if (!obstacle.HasVertex(vertexType))
            {
                var samplePoint = hit.collider.ClosestPointOnBounds(referencePoint);
                var directionFromAgentToSamplePoint = (samplePoint - transform.position).normalized;
                var scalar = Dot(originalDirection, directionFromAgentToSamplePoint);
                var magnitudeProduct = originalDirection.magnitude * directionFromAgentToSamplePoint.magnitude;
                var angle = Acos(scalar / magnitudeProduct);

                if (TryGetBestReachablePoint(out var reachablePoint, directionFromAgentToSamplePoint, -angle, hit.transform.gameObject.GetInstanceID()))
                {
                    var isVertex = Distance(samplePoint, reachablePoint) < 0.01f;
                    if (isVertex) obstacle.SetVertex(vertexType, samplePoint);

                    var nodePoint = reachablePoint + hit.normal + direction;
                    PointFound?.Invoke(nodePoint);
                }
            }
        }

        public bool IsConnectionPossibleAndPerceivable(Vector3 pos1, Vector3 pos2)
        {
            var dir = (pos1 - pos2).normalized;
            var distance = Distance(pos1, pos2);
            var nodesCanBeConnected = !Raycast(pos2, dir, distance, obstaclesLayer);
            if (nodesCanBeConnected)
            {
                dir = -dir;
                nodesCanBeConnected = !Raycast(pos1, dir, distance, obstaclesLayer);
            }
            return nodesCanBeConnected;
        }

        private bool TryGetBestReachablePoint(out Vector3 bestFitPoint, Vector3 startingDirection, float maxAngle, int targetID)
        {
            var step = 1f / scanPrecision;
            for (float alpha = 0f; alpha < 1; alpha += step)
            {
                var direction = startingDirection;
                direction.RotateYaw(maxAngle * alpha);
                if (Raycast(transform.position, direction, out var hit, depth, obstaclesLayer) && hit.transform.gameObject.GetInstanceID() == targetID)
                {
                    bestFitPoint = hit.point;
                    return true;
                }
            }
            bestFitPoint = zero;
            return false;
        }
    }
}
