﻿using UnityEngine;

namespace AgentX
{
    public static class Vertex
    {
        public enum Type
        {
            ForwardLeft,
            ForwardRight,
            BackwardLeft,
            BackwardRight
        }

        public static Type GetVertexType(Vector3 vertexPointInWorld, Transform transform)
        {
            vertexPointInWorld -= transform.position;
            var isRight = vertexPointInWorld.x > 0;
            var isForward = vertexPointInWorld.z > 0;
            return isForward ? (isRight ? Type.ForwardRight : Type.ForwardLeft) : (isRight ? Type.BackwardRight : Type.BackwardLeft);
        }
    }
}
