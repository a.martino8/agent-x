﻿using System.Collections.Generic;
using UnityEngine;

namespace AgentX
{
    public class Node
    {
        private static int IDCount;

        public readonly Vector3 position;
        public readonly List<Node> connectedNodes;
        public readonly int id;
        public float cost;
        public Node visitingParent;
        public bool wasVisited;

        public Node(Vector3 position)
        {
            this.position = position;
            connectedNodes = new List<Node>();
            id = IDCount++;
        }
    }
}
