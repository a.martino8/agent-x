using UnityEngine;
using static UnityEngine.Quaternion;
using static UnityEngine.Vector3;
using static UnityEngine.Time;
using static UnityEngine.Mathf;
using System.Collections;
using System;

namespace AgentX.Agent
{
    [Serializable]
    public class Movement
    {
        [SerializeField] private Transform transform = default;
        [SerializeField] private float movementSpeed = 1;
        [SerializeField] private float rotationSpeed = 1;

        public IEnumerator MoveTowardsRoutine(Vector3 destination)
        {
            var distance = Distance(destination, transform.position);
            var duration = distance / movementSpeed;
            var t = 0f;
            var initialPosition = transform.position;
            while (t < duration)
            {
                yield return null;
                t += deltaTime;
                transform.position = Lerp(initialPosition, destination, t/duration);
            }
            transform.position = destination;
        }

        public IEnumerator LookAtRoutine(Vector3 targetPoint)
        {
            targetPoint.y = transform.position.y;
            yield return FaceTowardsRoutine((targetPoint - transform.position).normalized);
        }

        public IEnumerator FaceTowardsRoutine(Vector3 direction)
        {
            var distance = Distance(direction, transform.forward);
            var duration = distance / rotationSpeed;

            var initialRotation = transform.rotation;
            var targetRotation = LookRotation(direction);
            for (float t = 0f; t < duration; t += deltaTime)
            {
                transform.rotation = Lerp(initialRotation, targetRotation, t / duration);
                yield return null;
            }
            transform.rotation = targetRotation;
        }
    }
}
