﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AgentX.Agent
{
    public class Obstacle
    {
        public IReadOnlyList<Vector3?> Vertexes => vertexes;
        public bool IsVertexComplete => vertexRecorded == vertexes.Length;
        public bool IsNormalComplete => normalsRecorded == normals.Length;
        private readonly Vector3?[] vertexes = new Vector3?[4];
        private readonly Vector3?[] normals = new Vector3?[4];
        private int vertexRecorded, normalsRecorded;

        public void SetVertex(Vertex.Type vertex, Vector3 vertexPoint)
        {
            vertexes[(int)vertex] = vertexPoint;
            vertexRecorded++;
        }

        public bool ContainsNormal(Vector3 normal)
        {
            return normals.Contains(normal);
        }

        public void SetNormal(Vector3 normal)
        {
            normals[normalsRecorded++] = normal;
        }

        public bool TryGetVertexPoint(Vertex.Type vertexType, out Vector3 vertexPoint)
        {
            var v = vertexes[(int)vertexType];
            var hasVertex = v != null;
            vertexPoint = hasVertex ? v.Value : Vector3.zero;
            return hasVertex;
        }

        public bool HasVertex(Vertex.Type vertexType) => vertexes[(int)vertexType] != null;
    }
}
