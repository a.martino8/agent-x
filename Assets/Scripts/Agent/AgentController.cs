using System.Collections;
using UnityEngine;
using static UnityEngine.Gizmos;
using static UnityEngine.Color;
using System.Collections.Generic;
using UnityEditor;

namespace AgentX.Agent
{
    public class AgentController : MonoBehaviour
    {
        [SerializeField] private AStarSolver solver = default;
        [SerializeField] private Movement movement = default;
        [SerializeField] private Perception perception = default;
        [SerializeField] private Transform target = null;

        [Header("Debug")]
        [SerializeField] private Color nodeConnectionColor = default;
        private readonly List<Node> map = new List<Node>();
        private Node currentNode;

        private void Start() => StartCoroutine(SearchRoutine());

        private IEnumerator SearchRoutine()
        {
            currentNode = new Node(transform.position);
            map.Add(currentNode);
            perception.PointFound += OnPointFound;

            var perceptionCoroutine = StartCoroutine(perception.Perceive());
            for (int i = 0; i < 4; i++) yield return movement.FaceTowardsRoutine(transform.right);
            currentNode.wasVisited = true;

            var adaptedPosition = target.position;
            adaptedPosition.y = transform.position.y;
            var isTargetFound = false;
            while (!isTargetFound)
            {
                var nextSteps = solver.Solve(currentNode, adaptedPosition, out isTargetFound);
                if (nextSteps == null) break;
                foreach (var node in nextSteps)
                {
                    currentNode = node;
                    StartCoroutine(movement.LookAtRoutine(node.position));
                    yield return movement.MoveTowardsRoutine(node.position);
                    if (!node.wasVisited)
                    {
                        node.wasVisited = true;
                        for (int i = 0; i < 4; i++) yield return movement.FaceTowardsRoutine(transform.right);
                    }
                }

                yield return null;
            }
            StopCoroutine(perceptionCoroutine);

            if (isTargetFound) Debug.Log("Success!");
            else Debug.Log("Failure!");
        }

        private void OnPointFound(Vector3 newPoint)
        {
            var newNode = new Node(newPoint);
            foreach (var node in map)
            {
                var canConnect = perception.IsConnectionPossibleAndPerceivable(newPoint, node.position);
                if (canConnect)
                {
                    node.connectedNodes.Add(newNode);
                    newNode.connectedNodes.Add(node);
                }
            }
            map.Add(newNode);
        }

        private void OnDrawGizmos()
        {
            if (map != null)
            {
                foreach (var node in map)
                {
                    color = node.wasVisited ? red : green;
                    DrawSphere(node.position, 0.25f);
                    Handles.Label(node.position + Vector3.up * 0.5f, $"{node.id}");
                    foreach (var connectedNode in node.connectedNodes)
                    {
                        color = nodeConnectionColor;
                        DrawLine(node.position, connectedNode.position);
                    }
                }
            }
        }
    }
}

